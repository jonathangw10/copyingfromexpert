//Original code Elena Cardiel Fab Academy 2019
//Fab Academy 2020 - Fab Lab León
//Servo                           
//Adrianino
//ATtiny1614

#include <Servo.h>


Servo myservo;  // crea el objeto servo
 
int pos = 0;    // posicion del servo
 
void setup() {

  myservo.attach(0);  // vincula el servo al pin digital PA4
  
}
 
void loop() {
   //varia la posicion de 0 a 160, con esperas de 30ms
   for (pos = 0; pos <= 160; pos += 2) 
   {
      myservo.write(pos);              
      delay(80);                       
   }
 
   //varia la posicion de 0 a 160, con esperas de 30ms
   for (pos = 160; pos >= 0; pos -= 2) 
   {
      myservo.write(pos);              
      delay(80);                       
   }
}
