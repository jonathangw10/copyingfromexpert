//Fab Academy 2021 - Fab Lab León
//Adrián Torres & Marta Verde
//3 axis Accelerometer
//Adrianino
//ATtiny1614

import processing.serial.*;

Serial myPort;  // The serial port
int cycles = 0;

float in_x;
float in_y; 
float in_z;
float x, y, z;
color[] col = new color[6];

void setup() {
  size(700, 600); //size of the window
  myPort = new Serial(this, "COM4", 115200);
  myPort.bufferUntil('\n');   // don't generate a serialEvent() unless you get a newline character:
  initGraph();  //we start variable for graphs
}

void draw() {
  background(0);   // set inital background black
  
  line(0,500,700,500);     
  fill(255);
  rect(120,100,450,100); //we create the different white boxes.
  rect(120,250,450,100);
  rect(120,400,450,100);
 
  //text
  fill(255);
  textSize(16);
  text("X Axis", 50, 150);
  text("Y Axis", 50, 300);
  text("Z Axis", 50, 450);
  textSize(20);
  text(in_x, 600, 150);
  text(in_y, 600, 300);
  text(in_z, 600, 450);

 
  //x axis
  fill(col[1]);
  x = map(in_x, 0, 200, 0, 400);
  rect(120, 100, 225-x, 100);
  
  //y axis
  fill(col[2]);
  y = map(in_y, 0, 200, 0, 400);
  rect(120, 250, 225-y, 100);

  //z axis
  fill(col[3]);
  z = map(in_z, 0, 200, 0, 400);
  rect(120, 400, 225-z, 100);
}

void initGraph() {          //the colors of the graph
  col[0] = color(255, 127, 31);
  col[1] = color(255, 0, 0);
  col[2] = color(0, 255, 0);
  col[3] = color(0, 0, 255);
  col[4] = color(127, 255, 31);
  col[5] = color(127);
}

void serialEvent(Serial myPort) {
  String inString = myPort.readStringUntil('\n');
  if (inString != null) {
    inString = trim(inString);
    float[] values = float(split(inString, ","));
    if (values.length >=3) {
      in_x = values[0]; 
      in_y = values[1];
      in_z = values[2];
    }
  }
}


 
