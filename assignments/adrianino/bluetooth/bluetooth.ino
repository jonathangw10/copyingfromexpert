//Fab Academy 2021 - Fab Lab León
//Bluetooth                          
//Adrianino
//ATtiny1614

const int led = 8; // LED pin

int option;
 
void setup(){
  Serial.begin(9600);  //speed of the communications
  pinMode(led, OUTPUT); 
}

void loop(){
  //if there is pending information
  if (Serial.available()>0){
    //let's read the option
    char option = Serial.read();
    //if the option is between '1' and '9'
    if (option >= '1' && option <= '9')
    {
      //we subtract the value '0' to obtain the number sent
      option -= '0';
      for(int i=0;i<option;i++){
         digitalWrite(led, HIGH);
         delay(100);
         digitalWrite(led, LOW);
         delay(200);
      }
    }
  }
}    
