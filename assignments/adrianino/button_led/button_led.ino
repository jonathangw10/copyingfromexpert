//Fab Academy 2020 - Fab Lab León
//Button + LED                           
//Adrianino
//ATtiny1614
//
//Original code:Neil Gershenfeld 12/8/19
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//


const int ledPin1 = 8;//first light
const int buttonPin = 10;// button pin
int buttonState = 0;//initial state of the button
int i = 0; //variable intensity led

void setup() { //declaration of inputs and outputs
  pinMode(ledPin1, OUTPUT);
  pinMode(buttonPin, INPUT);
}
void loop() {
  buttonState = digitalRead(buttonPin);// we read the state of the button
  if (buttonState == HIGH) { //if we press the button
  digitalWrite(ledPin1, HIGH);
  delay(500);                       
  digitalWrite(ledPin1, LOW);    
  delay(500);
  digitalWrite(ledPin1, HIGH);
  delay(500);                       
  digitalWrite(ledPin1, LOW);    
  delay(500);
  digitalWrite(ledPin1, HIGH);
  delay(2000);                        
  digitalWrite(ledPin1, LOW);    
  delay(1000);
  
}
  else {  //if we don't press the button
  digitalWrite(ledPin1, LOW);
  }
}
