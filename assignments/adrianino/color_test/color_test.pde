//Fab Academy 2021 - Fab Lab León
//Adrián Torres & Marta Verde
//Color
//Adrianino
//ATtiny1614

import processing.serial.*;
Serial myPort;        // The serial port
int cycles = 0;
float r, g, b, w;
float rMap, gMap, bMap, wMap;
int margin;
int barHeight;

void setup () {
  size(600, 400); 
  printArray(Serial.list());
  myPort = new Serial(this, "COM4", 115200);
 // myPort = new Serial(this, "/dev/tty.usbmodem1411", 115200);
  
  // don't generate a serialEvent() unless you get a newline character:
  myPort.bufferUntil('\n');
  // set inital background:
  background(0);
  margin = 30;
  barHeight = 55;
}
 void draw () { 
  background(0);
  noStroke();
 
  fill(255,0,0);
  rect(margin, height/4, rMap, barHeight);
  fill(0,255,0);
  rect( margin, height/4 + barHeight, gMap, barHeight);
  fill(0,0,255);
  rect( margin, height/4 + barHeight*2, bMap,barHeight);
  fill(255);
  rect(margin, height/4 + barHeight*3, wMap, barHeight);
  /*
  if (cycles >= 10) {
    cycles = 0;
    background(0); 
   }
   cycles++;
   */
 }
 
void serialEvent(Serial myPort) {
  String inString = myPort.readStringUntil('\n');
  if (inString != null) {
    inString = trim(inString);
    int[] data = int(split(inString, ",")); //se parte el mensaje con las comas
    if (data.length >=4) { //este numero, poner cuantos mensajes se esperan
      r = int(data[0]);
      g = int(data[1]);
      b = int(data[2]);
      w = int(data[3]);
      
      rMap = map(r, 0, 6000, 0, height);
      gMap = map(g, 0, 6000, 0, height);
      bMap = map(b, 0, 6000, 0, height);
      wMap = map(w, 0, 6000, 0, height);
    }
  }
} 
 
 
 
