//Fab Academy 2020 - Fab Lab León
//Motor
//Adrianino
//ATtiny1614
const int switch1Pin = 10;     // switch 1        
const int motor1Pin = 0;      // H-bridge pin 0 (in2) 
const int motor2Pin = 1;      // H-bridge pin 1 (in1) 

void setup() {
    // set the switch pins as input pins and activate their internal pull up resistors
    // so they are not in a floating state because their default state is now HIGH
    pinMode(switch1Pin, INPUT); 
   

    // set H-bridge pins as outputs:
    pinMode(motor1Pin, OUTPUT);
    pinMode(motor2Pin, OUTPUT);
  }

void loop() {
    // if switch1 is pressed, (=LOW because the unpressed 'pulled up' state is HIGH)
   
  if (digitalRead(switch1Pin) == LOW) {
      analogWrite(motor1Pin, 127); // set pin 1 of the H-bridge to 50% using PWM
      analogWrite(motor2Pin, 0);   // set pin 2 of the H-bridge to low state
    }
     // if neither of the switches are pressed, the motor will stand still
  else
      {
      digitalWrite(motor1Pin, LOW);   // set pin 1 of the H-bridge low
      digitalWrite(motor2Pin, LOW);   // set pin 2 of the H-bridge low
      }
  }
