//Fab Academy 2020 - Fab Lab León
//NTC
//Adrianino
//ATtiny1614
int sensorPin = 0;    // analog input pin to hook the sensor to
int sensorValue = 0;  // variable to store the value coming from the sensor
 
void setup() {
  Serial.begin(115200); // initialize serial communications
}
 
void loop() {
  sensorValue = analogRead(sensorPin); // read the value from the sensor
  sensorValue = map(sensorValue, 482, 890, 19, 180);
  Serial.println(sensorValue); // print value to Serial Monitor
  //mySerial.println("x"); // print value "x" to Serial Monitor
  delay(500); // short delay so we can actually see the numbers
}
