//Fab Academy 2020 - Fab Lab León
//PIR sensor or Doppler radar
//Adrianino
//ATtiny1614


const int RadarPin = 0; // digital input pin to hook the sensor to
int value = 6; // variable to store the value coming from the sensor
void setup()
{
  Serial.begin(115200); // initialize serial communications
  pinMode(RadarPin, INPUT); //digital input pin
}
 
void loop()
{
  int value= digitalRead(RadarPin); // read the value from the sensor (0 or 1)
  Serial.println(value);  // print value to Serial Monitor
}
