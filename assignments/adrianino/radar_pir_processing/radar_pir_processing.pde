//Fab Academy 2020 - Fab Lab León
//PIR
//Adrianino
//ATtiny1614

import processing.serial.*;
float val;     
Serial myPort;


 
void setup() {
  size(500, 250); //size of the square
  background(255); //background
  println(Serial.list()); //port list
  //replace the port String with the port where your Arduino is connected
  //myPort = new Serial(this, "/dev/tty.wchusbserial1450", 115200);
  myPort = new Serial(this, "COM4", 115200); //serial port. IMPORTANT to check
  myPort.bufferUntil('\n');
  frameRate(10); //speed of frame rate 10 seconds
}
void draw() {
  background(255); //background
    rect(250,0, 250, 250);
    fill(0,255,0); 
  println(val); // in the serial we saw the data
  
  
  if (val == 1) {
    // detect the sensor
    fill(0, 0, 0);
    textSize(32);
    text("motion", 75, 125); 
    fill(255,0,0);
 
  } else {
    // not detection 
    fill(0, 0, 0);
    textSize(32);
    text("off", 100, 125); 
    fill(0,255,0); 
  }
}

void serialEvent(Serial myPort) {
  String inString = myPort.readStringUntil('\n');
  if (inString != null) {
    inString = trim(inString);
    float[] values = float(split(inString, ","));
    if (values.length >=1) {
      val = values[0];
    }
  }
}
