//Original code Mauro Herrero Fab Academy 2021
//Fab Academy 2021 - Fab Lab León
//Stepper Motor                         
//Adrianino
//ATtiny1614

#include <util/delay.h>
#define onDelay() _delay_us(12) //PWM on time
#define offDelay() _delay_us(38) //PWM off time
#define pwmCount 100 // number of PWM cycles, total time 5000 us - 5 milliseconds

const int blue = 0;
const int red = 1;
const int green = 2;
const int black = 3;
const int pulseDelay = 10;
int stepsPerRev = 200;


void setup() {
  // put your setup code here, to run once:
  pinMode(blue, OUTPUT);
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(black, OUTPUT);
}

void ApBp() {
  digitalWrite(red, LOW);
  digitalWrite(black,LOW);
  for (int count = 0; count < pwmCount; ++count){
    digitalWrite(blue, HIGH);
    digitalWrite(green, HIGH);
  }
}

void AmBp() {
  digitalWrite(blue, LOW);
  digitalWrite(black, LOW);
  for (int count = 0; count < pwmCount; ++count){
    digitalWrite(red, HIGH);
    digitalWrite(green, HIGH);
  }
}

void AmBm() {
  digitalWrite(blue, LOW);
  digitalWrite(green, LOW);
  for (int count = 0; count < pwmCount; ++count){
    digitalWrite(red, HIGH);
    digitalWrite(black, HIGH);
  }
}

void ApBm() {
  digitalWrite(red, LOW);
  digitalWrite(green, LOW);
  for (int count = 0; count < pwmCount; ++count){
    digitalWrite(blue, HIGH);
    digitalWrite(black, HIGH);
  }
}

void stepCW() {
  ApBp();
  AmBp();
  AmBm();
  ApBm();
}

void stepCCW(){
  ApBm();
  AmBm();
  AmBp();
  ApBp();
}
void loop() {
  // put your main code here, to run repeatedly:
  
  for (int i = 0; i<stepsPerRev; i++) {
    stepCW();  
  }
  delay(1000);
  for (int i = 0; i<stepsPerRev; i++) {
    stepCCW();
  }
  delay(1000);
}
