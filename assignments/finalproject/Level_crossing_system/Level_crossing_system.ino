//Adrián Torres. Fab Academy 2020
//ATtiny1614
//Fab Lab León

#include <SoftwareSerial.h>
#include <Servo.h>
Servo myservo1;  // create servo object to control a servo1
Servo myservo2;  // create servo object to control a servo2
int sensorPin1 = A0;    // analog input pin to hook the sensor to
int sensorPin2 = A1;    // analog input pin to hook the sensor to
int sensorValue1 = 0;  // variable to store the value coming from the sensor1
int sensorValue2 = 0;  // variable to store the value coming from the sensor2
int ledPin1 = 13;//first light
int posinicial = 60;    // initial position of the servos
int posfinal = 30;
int pos = posinicial;
int umbral = 100;  //minimum value of the sensor to activate the system

void setup() {
  Serial.begin(115200);
  myservo1.attach(3);  // attaches the servo on PA4 to the servo object
  myservo2.attach(6);  // attaches the servo on PA5 to the servo object
  pinMode(ledPin1, OUTPUT);
  myservo1.write(posinicial); //initial position of the servo
  myservo2.write(posinicial); //initial position of the servo
}
 

 
void loop() {

  sensorValue1 = analogRead(sensorPin1); // read the value from the sensor
  sensorValue1 = map(sensorValue1, 0, 1024, 1024, 0);
  sensorValue2 = analogRead(sensorPin2); // read the value from the sensor
  sensorValue2 = map(sensorValue2, 0, 1024, 1024, 0);

  //values in serial
  Serial.print("pos: ");
  Serial.print(pos);
  Serial.print("Valor 1:");
  Serial.print(sensorValue1);
  Serial.print("  ");
  Serial.print("Valor 2:");
  Serial.println(sensorValue2);
  //Serial.clear();
  
  
  if (sensorValue1 <= umbral) { //if the value is less than the threshold
   digitalWrite(ledPin1, HIGH);  //active the lights of the level crossing
   
   for (pos ; pos >= posfinal; pos -= 2) { //close the barrier 
      myservo1.write(pos); 
      myservo2.write(pos);
     delay(80);
    }
 
  }

     if (sensorValue2 <= umbral) { //if the value is less than the threshold
      digitalWrite(ledPin1, LOW);   //turn off the lights of the level crssing
      for (pos; pos <= posinicial; pos += 2) {   //open the barrier
      myservo1.write(pos);              
      myservo2.write(pos);  
      delay(80);
      }  
    }  

}        

/*si ves el ejemplo que pensamos para networking, tienes que establecer diferentes escenarios para tu montaje:
1 El tren no ha pasado
2 El tren esta pasando
3 El tren ha pasado


Asi en tu programa tiene que haber una variable que almacene el estado del sistema. Vamos a llamarla ESTADO

==> Tren ==> sensor 1 ==> BArrera ==> Sensor 2 ==> Final

ESTADO va a depender de todos los sensores, asi que tendremos una parte de evaluacion:
Si sensor 1 esta >20 y sensor 2 >20 entonces ESTADO = 1
Si sensor 1 es <20 y sensor 2 > 20 entoncess ESTADO = 2  (ojo, que si el tren es muy largo taparia los dos a la vez)
Si sensor 1 es >20 y sensor 2 <20 entonces ESTADO = 3

Y estos tres estados no se dan a la vez,con lo que puede hacerse una seleccion de codigo
Leete la documentacion de el codigo para SWITCH...CASE deArduino:
https://www.arduino.cc/reference/en/language/structure/control-structure/switchcase/

Entonces el codigo final tendria esta pinta:

1º Evualo los dos sensores, para establecer en que estado se encuentra el montaje
2º Alamceno en una variable ESTADO como esta la maqueta en ese momento
3º HAcer un switch case para decidir que hacer en cada uno de los estados
4º Repetir
*/
