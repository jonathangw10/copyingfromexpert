//Fab Academy 2020 - Fab Lab León
//Phototransistor IR
//SAMDino
//SAMD11C
int sensorPin = 4;    // analog input pin to hook the sensor to
int sensorValue = 0;  // variable to store the value coming from the sensor
 
void setup() {
  Serial.begin(115200); // initialize serial communications
}
 
void loop() {
  sensorValue = analogRead(sensorPin); // read the value from the sensor
  sensorValue = map(sensorValue, 40, 60, 0, 1024);
  Serial.println(sensorValue); // print value to Serial Monitor
  //mySerial.println("x"); // print value "x" to Serial Monitor
  delay(500); // short delay so we can actually see the numbers
}
