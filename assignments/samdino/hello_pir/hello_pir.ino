//Fab Academy 2020 - Fab Lab León
//PIR sensor                            
//SAMDino
//SAMD11C

//Original code by Luis Llamas.

const int LEDPin = 2;        // for LED
const int PIRPin = 5;         // for PIR sensor
 
int pirState = LOW;           // start there is no movement
int val = 0;                  // pin status
 
void setup() 
{
   pinMode(LEDPin, OUTPUT); 
   pinMode(PIRPin, INPUT);
   Serial.begin(115200);
}
 
void loop()
{
   val = digitalRead(PIRPin);
   if (val == HIGH)   //if activated
   { 
      digitalWrite(LEDPin, HIGH);  //LED ON
      if (pirState == LOW)  
      {
        Serial.println("1");
        pirState = HIGH;
      }
   } 
   else   //is not activated
   {
      digitalWrite(LEDPin, LOW); // LED OFF
      if (pirState == HIGH)  //si
      {
        Serial.println("0");
        pirState = LOW;
      }
   }
}
