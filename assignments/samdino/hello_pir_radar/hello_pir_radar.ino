//Fab Academy 2020 - Fab Lab León
//PIR sensor or Doppler radar
//SAMDino
//SAMD11C


const int RadarPin = 4; // digital input pin to hook the sensor to
int value = 0; // variable to store the value coming from the sensor
void setup()
{
  Serial.begin(115200); // initialize serial communications
  pinMode(RadarPin, INPUT); //digital input pin
}
 
void loop()
{
  int value= digitalRead(RadarPin); // read the value from the sensor (0 or 1)
  Serial.println(value);  // print value to Serial Monitor
}
