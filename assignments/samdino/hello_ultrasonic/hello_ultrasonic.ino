//Fab Academy 2020 - Fab Lab León
//Ultrasonic sensor
//SAMDino
//SAMD11C



const int EchoPin = 4;
const int TriggerPin = 5;
 
void setup() {
   Serial.begin(115200);
   pinMode(TriggerPin, OUTPUT);
   pinMode(EchoPin, INPUT);
}
 
void loop() {
   int cm = ping(TriggerPin, EchoPin);
  Serial.print("");
   Serial.println(cm);
   delay(1000);
}
 
int ping(int TriggerPin, int EchoPin) {
   long duration, distanceCm;
   
   digitalWrite(TriggerPin, LOW);  //to generate a clean pulse we set LOW 4us
   delayMicroseconds(4);
   digitalWrite(TriggerPin, HIGH);  //we generate Trigger (trigger) of 10us
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);
   
   duration = pulseIn(EchoPin, HIGH);  //we measure the time between pulses, in microseconds
   
   distanceCm = duration * 10 / 292/ 2;   //we convert distance, in cm
   return distanceCm;
}
