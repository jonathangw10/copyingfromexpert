//Fab Academy 2020 - Fab Lab León
//Motor
//SAMDino
//SAMD11C
       
const int motor1Pin = 4;      // H-bridge pin 0 (in2) 
const int motor2Pin = 5;      // H-bridge pin 1 (in1) 

void setup() {

    // set H-bridge pins as outputs:
    pinMode(motor1Pin, OUTPUT);
    pinMode(motor2Pin, OUTPUT);
  }

void loop() {
   // change the direction the motor spins by talking to the control pins
  // on the H-Bridge

    digitalWrite(motor1Pin, HIGH);    // Turn the motor in one direction
    digitalWrite(motor2Pin, LOW);
    
    delay(2000);                      // Wait two seconds

    digitalWrite(motor1Pin, LOW);     // Switch off the motor
    digitalWrite(motor2Pin, LOW);

    delay(3000);                      // Wait three seconds

    digitalWrite(motor1Pin, LOW);
    digitalWrite(motor2Pin, HIGH);    // Turn the motor in the other direction
 

    delay(2000);                      // Wait two seconds

    digitalWrite(motor1Pin, LOW);     // Switch off the motor
    digitalWrite(motor2Pin, LOW);

    delay(3000);                      // Wait three seconds
  }
