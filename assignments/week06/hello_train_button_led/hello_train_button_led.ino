const int ledPin1 = 3;//first light
const int ledPin2 = 6;//white light
const int ledPin3 = 7;//white light
const int ledPin4 = 1;//red light
const int ledPin5 = 2;//red light
const int buttonPin = 0;
int buttonState = 0;

void setup() {
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(ledPin3, OUTPUT);
  pinMode(ledPin4, OUTPUT);
  pinMode(ledPin5, OUTPUT);
  pinMode(buttonPin, INPUT);
}
void loop() {
  buttonState = digitalRead(buttonPin);
  if (buttonState == HIGH) {
  digitalWrite(ledPin2, HIGH);
  digitalWrite(ledPin3, HIGH);
  delay(2000);                       
  digitalWrite(ledPin4, HIGH);
  digitalWrite(ledPin5, HIGH);    
  delay(2000);
  digitalWrite(ledPin1, HIGH);
  delay(5000);                       
  digitalWrite(ledPin1, LOW);    
  delay(2000);
  digitalWrite(ledPin4, LOW);
  digitalWrite(ledPin5, LOW);
  delay(2000);                       
  digitalWrite(ledPin2, LOW); 
  digitalWrite(ledPin3, LOW);    
  delay(1000);
  
}
  else {
  digitalWrite(ledPin1, LOW);
  digitalWrite(ledPin2, LOW);
  digitalWrite(ledPin3, LOW);
  digitalWrite(ledPin4, LOW);
  digitalWrite(ledPin5, LOW);
  }
}
