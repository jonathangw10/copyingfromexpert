//Adrián Torres - Fab Academy 2020 - Fab Lab León
//tiny412 hello_blynk
//
//Original code:Neil Gershenfeld 12/8/19
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//





int ledpwm = 3;//pinout of the attiny412

int i = 0;//variable that will make the work cycle increase

void setup(){
 pinMode(ledpwm,OUTPUT); 
}
void loop(){
  for(i = 0; i <= 255 ; i++){
   analogWrite(ledpwm,i); 
   analogWrite(5,255-i);
   delay(25);
  }
   for(i = 255; i>= 0 ; i--){
   analogWrite(ledpwm,i); 
   analogWrite(5,255-i);
   delay(25);
  }
  
}
