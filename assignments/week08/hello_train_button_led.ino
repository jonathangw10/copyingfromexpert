//Adrián Torres - Fab Academy 2020 - Fab Lab León
//tiny1614 hello_train
//
//Original code:Neil Gershenfeld 12/8/19
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//


const int ledPin1 = 3;//first light
const int ledPin2 = 6;//white light
const int ledPin3 = 7;//white light
const int ledPin4 = 1;//red light
const int ledPin5 = 2;//red light
const int buttonPin = 0;// button pin
int buttonState = 0;//initial state of the button
int i = 0; //variable intensity led

void setup() { //declaration of inputs and outputs
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(ledPin3, OUTPUT);
  pinMode(ledPin4, OUTPUT);
  pinMode(ledPin5, OUTPUT);
  pinMode(buttonPin, INPUT);
}
void loop() {
  buttonState = digitalRead(buttonPin);// we read the state of the button
  if (buttonState == HIGH) { //if we press the button
  digitalWrite(ledPin2, HIGH);
  digitalWrite(ledPin3, HIGH);
  delay(2000);                       
  digitalWrite(ledPin4, HIGH);
  digitalWrite(ledPin5, HIGH);    
  delay(2000);
  digitalWrite(ledPin3, HIGH);    
  delay(5000);
  digitalWrite(ledPin3, LOW);    
  delay(1000);
  digitalWrite(ledPin4, LOW);
  digitalWrite(ledPin5, LOW);
  delay(2000);                       
  digitalWrite(ledPin2, LOW); 
  digitalWrite(ledPin3, LOW);    
  delay(1000);
  
}
  else {  //if we don't press the button
  digitalWrite(ledPin1, LOW);
  digitalWrite(ledPin2, LOW);
  digitalWrite(ledPin3, LOW);
  digitalWrite(ledPin4, LOW);
  digitalWrite(ledPin5, LOW);
  }
}
