/******************************************************************************
QRD1114_Proximity_Example.ino
Example sketch for SparkFun's QRD1114 Reflectance Proximity Sensor
  (https://www.sparkfun.com/products/246)
Jim Lindblom @ SparkFun Electronics
May 2, 2016

Modified by Adrián Torres Omaña
Fab Academy 2020 - Fab Lab León

ATtiny1614

******************************************************************************/
const int Sensor = 1; // Sensor output voltage

void setup() 
{
  Serial.begin(115200);
  pinMode(Sensor, INPUT);
}

void loop() 
{
  // Read in the ADC and convert it to a voltage:
  int proximityADC = analogRead(Sensor);
  float proximityV = (float)proximityADC * 5.0 / 1023.0;
  Serial.println(proximityV);
  delay(100);
}
