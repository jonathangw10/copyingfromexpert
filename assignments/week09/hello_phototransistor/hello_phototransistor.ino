//Original code: Elena Cardiel. Fab Academy 2019.
//
//Modified by Adrián Torres. Fab Academy 2020.
//
//ATtiny1614

int sensorPin = 1;    // analog input pin to hook the sensor to
int sensorValue = 0;  // variable to store the value coming from the sensor
 
void setup() {
  Serial.begin(115200); // initialize serial communications
}
 
void loop() {
  sensorValue = analogRead(sensorPin); // read the value from the sensor
  sensorValue = map(sensorValue, 0, 1024, 1024, 0);
  Serial.println(sensorValue); // print value to Serial Monitor
  //mySerial.println("x"); // print value "x" to Serial Monitor
  delay(500); // short delay so we can actually see the numbers
}
