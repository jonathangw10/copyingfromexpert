//Adrián Torres Fab Academy 2020


int ledPin1 = 0; //led1
int ledPin2 =1; //led2

void setup() {
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
}
 
void loop() {
     digitalWrite(ledPin1, HIGH);
     digitalWrite(ledPin2, LOW);
     delay(750); 
     digitalWrite(ledPin1, LOW);
     digitalWrite(ledPin2, HIGH);
     delay(750);
}
