//Adrián Torres Fab Academy 2020

#include <Servo.h>


Servo myservo;  // crea el objeto servo
 
int pos = 0;    // posicion del servo
 
void setup() {

  myservo.attach(3);  // vincula el servo al pin digital PA7
}
 
void loop() {
   //varia la posicion de 8 a 37, con esperas de --ms
   for (pos = 8; pos <= 37; pos += 2) 
   {
      myservo.write(pos);              
      delay(80);                       
   }
 
   //varia la posicion de 8 a 37, con esperas de --ms
   for (pos = 37; pos >= 8; pos -= 2) 
   {
      myservo.write(pos);              
      delay(80);                       
   }
}
