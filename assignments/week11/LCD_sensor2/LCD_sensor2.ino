//Adrián Torres Fab Academy 2020
//Fab Lab León
//

#include <LiquidCrystal_PCF8574.h>
#include <Wire.h>

LiquidCrystal_PCF8574 lcd(0x27); // set LCD address 0x27

int sensorPin = 1;    // analog input pin to hook the sensor to
int sensorValue = 0;  // variable to store the value coming from the sensor
 

void setup() {
    Serial.begin(115200); // initialize serial communications
   lcd.begin(16,2); // initialize 16x2 LCD
   //lcd.begin(20,4); // initialize 20x4 LCD
   lcd.setBacklight(255); // turn on backlight
   lcd.home(); // go home
   lcd.clear(); // clear display
   lcd.print("Light!"); // print text
   lcd.setCursor(0,1); // move cursor
   //lcd.print("Lux: ");
   }

void loop() {
  sensorValue = analogRead(sensorPin); // read the value from the sensor
  sensorValue = map(sensorValue, 0, 1024, 1024, 0);
  Serial.println(sensorValue); // print value to Serial Monitor
   if (sensorValue <= 350) {  //if the value is equal to or less than 350
    lcd.clear();          // clear the LCD
    lcd.print("Light!"); // print text "Light"
    lcd.setCursor(4,1); //at the fourth cursor of the second line
    lcd.print("TURN ON"); //print text "TURN ON"
    delay(500);  
    }
   else{                  //if the value is equal to or more than 350
    lcd.clear();          // clear the LCD
    lcd.print("Light!"); // print text "Light"
    lcd.setCursor(4,1); //at the fourth cursor of the second line
    lcd.print("TURN OFF"); //at the fourth cursor of the second line
    delay(500);  
   }  
}
  
