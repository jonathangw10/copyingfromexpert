#include <SoftwareSerial.h>
SoftwareSerial mySerial(5,4); // TX, RX

int sensorPin = 1;    // analog input pin to hook the sensor to
int ledPin = 0;
int sensorValue = 0;  // variable to store the value coming from the sensor
 
void setup() {
  mySerial.begin(9600); // initialize serial communications
  pinMode (ledPin,OUTPUT);

}
 
void loop() {
  digitalWrite(ledPin, HIGH);
  sensorValue = analogRead(sensorPin); // read the value from the sensor
  sensorValue = map(sensorValue, 0, 1024, 1024, 0);
  mySerial.println(sensorValue); // print value to Serial Monitor
  //mySerial.println("x"); // print value "x" to Serial Monitor
  delay(500); // short delay so we can actually see the numbers
}
