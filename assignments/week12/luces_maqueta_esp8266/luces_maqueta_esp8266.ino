//Adrián Torres - Fab Academy 2020
//Fab Lab León

#include <ESP8266WiFi.h>
 
const char* ssid = "***************";//type your ssid
const char* password = "**********";//type your password

// Configuración de la IP estática.
    IPAddress local_IP(192, 168, 1, 12);
    IPAddress gateway(192, 168, 1, 1);
    IPAddress subnet(255, 255, 255, 0); 

int led1 = 2; // GPIO2 of ESP8266
int led2 = 0; // GPIO3 of ESP8266
WiFiServer server(80);
 
void setup() {
  Serial.begin(115200);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  
// Establecimiento de la IP estática.
   WiFi.config(local_IP, gateway, subnet);
  
// Conecta a la red wifi.
  Serial.println();
  Serial.print("Conectando con ");
  Serial.println(ssid);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Conectado con WiFi.");
 
  // Inicio del Servidor web.
  server.begin();
  Serial.println("Servidor web iniciado.");
 
  // Esta es la IP
  Serial.print("Esta es la IP para conectar: ");
  Serial.print("http://");
  Serial.println(WiFi.localIP());
}
 
void loop() {
  // Consulta si se ha conectado algún cliente.
  WiFiClient client = server.available();
  if (!client) {
    return;
  }

  Serial.print("Nuevo cliente: ");
  Serial.println(client.remoteIP());

  // Espera hasta que el cliente envíe datos.
  while(!client.available()){ delay(1); }

  /////////////////////////////////////////////////////
  // Lee la información enviada por el cliente.
  String req = client.readStringUntil('\r');
  Serial.println(req);
  
  // Realiza la petición del cliente.
       if (req.indexOf("on1") != -1) {digitalWrite(led1, HIGH);}
       if (req.indexOf("off1") != -1){digitalWrite(led1, LOW);}
       if (req.indexOf("on2") != -1) {digitalWrite(led2, HIGH);}
       if (req.indexOf("off2") != -1){digitalWrite(led2, LOW);}

  //////////////////////////////////////////////
  // Página WEB. ////////////////////////////
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println(""); //  Importante.

  Serial.print("Cliente desconectado: ");
    Serial.println(client.remoteIP());
    client.flush();
    client.stop();
}
