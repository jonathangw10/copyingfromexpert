//code made by Adrián Torres
//Fab Academy 2020
//Fab Lab León
//ATtiny412 bridge

#include <SoftwareSerial.h>
SoftwareSerial mySerial(2,3); //RX, TX
int v=0; 
int nodeid=1; //Node Identification
int i=0;

void setup() {
  mySerial.begin(115200); //initialize serial communications
  pinMode(4, OUTPUT); // led
}

void loop() {
  for (i=1;i<=3;i++){ // initialization; condition; increment
  mySerial.println(i); // print value to Serial
  delay(1000);
  }

  while (mySerial.available () == 0 ) {}  //while serial is 0 
  v = mySerial.parseInt();
  if(v == nodeid)        //If the value of v equals the identification of the node
{
  digitalWrite(4,HIGH);
  delay(200);
  digitalWrite(4,LOW);
  delay(200);
}
else
{
  digitalWrite(4,LOW);
 }
}
