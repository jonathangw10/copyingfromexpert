//code made by Adrián Torres
//Fab Academy 2020
//Fab Lab León
//ATtiny412 bridge


int v=0; 
int nodeid=1; //Node Identification

void setup() {
  Serial.begin(115200); //initialize serial communications
  pinMode(4, OUTPUT); // led
}

void loop() {
  while (Serial.available () == 0 ) {}
  v = Serial.parseInt();
  Serial.println(v);
  
  if(v == nodeid)   //If the value of v equals the identification of the node
{
  digitalWrite(4,HIGH);
  delay(1000);
  digitalWrite(4,LOW);
  delay(1000);
}
else
{
  digitalWrite(4,LOW);
 }
}
