//code made by Adrián Torres
//Fab Academy 2020
//Fab Lab León
//ATtiny412

#include <SoftwareSerial.h>
SoftwareSerial serial_led(2, 3); //tx, rx

int id, value; // this is going to be the variable that we read. 
int MyId = 1; //  variable that we will compare with what comes to us from the bus. The bus will send us a 1.

int ledPin1 = 0; //led1

 
void setup() {
  serial_led.begin(115200);
  pinMode(ledPin1, OUTPUT);

}


 
void loop(){
   int average;
   if (serial_led.available()){
    id = serial_led.parseInt();
    if (id == MyId){
    average = serial_led.parseInt();
    }
    else {
    average = 0;
    }
  }
    if (average > 100) {
       digitalWrite(ledPin1, HIGH); // led on
    }
    else if (average <= 100) {
      digitalWrite(ledPin1, LOW); // led off
  }
}
