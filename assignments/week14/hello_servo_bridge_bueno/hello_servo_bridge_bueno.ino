//Original code made by Paco Gonzalez. Fab Academy 2016 and Hugo F. Garcia. Fab Academy 2019.
//Modified Adrián Torres. Fab Academy 2020
//Fab Lab León
//ATtiny1614

#include <SoftwareSerial.h>
SoftwareSerial serial_servo(5,4); //tx, rx
#include <Servo.h>
Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

int id, value; // this is going to be the variable that we read. 
int MyId = 1; //  variable that we will compare with what comes to us from the bus. The bus will send us a 1.


//int pos = 0;    // variable to store the servo position

void setup() {
  serial_servo.begin(115200);
  myservo.attach(3);  // attaches the servo on pin 3 to the servo object
 
}

void loop(){
  int value;
   if (serial_servo.available()){
    id = serial_servo.parseInt();
    if (id == MyId){
    value = serial_servo.parseInt();
    }
    else {
    value = 0;
    }
  }
  if (value > 0) { // if we are receiving data from de sensor...
  int angle=map(value,0,1024,0,120); //the servo angle, using the values from 0 to 1024 and from 0 to 120 degrees.
  myservo.write(angle);
  }
}
