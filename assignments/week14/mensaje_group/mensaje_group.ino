//code made by Adrián Torres
//Fab Academy 2020
//Fab Lab León
//ATtiny1614

#include <SoftwareSerial.h>

int sensorPin = 1;    // analog input pin to hook the sensor to
int sensorValue = 0;  // variable to store the value coming from the sensor

int adress = 1;
 
void setup() {
  Serial.begin(115200); // initialize serial communications
}
 
void loop() {
  sensorValue = analogRead(sensorPin); // read the value from the sensor
  sensorValue = map(sensorValue, 0, 1024, 1024, 0);
  if (sensorValue <= 20) {
    Serial.print(adress);
    Serial.print(","); 
    Serial.println ("TRAIN PASSING"); //the train is passing.
    delay(500);
    }
}
